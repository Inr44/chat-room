#include "server.h"

pthread_mutex_t shared_buffer_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t new_message = PTHREAD_COND_INITIALIZER;
pthread_mutex_t client_list_mutex = PTHREAD_MUTEX_INITIALIZER;


void generate_final_message(char *final_message, char *message, char *sender_nickname){
    sprintf (final_message, "[%s]: %s", sender_nickname, message);
}

void generate_private_message(char *final_message, char *message, char *sender_nickname){
    char buffer[1024], target_client[NICK_LEN];
    sscanf(message, "%*s %s %[^\n]", target_client, buffer);
    sprintf (final_message, "[%s (privado) a %s]: %s", sender_nickname, target_client, buffer);
}

void private_message(shared_buffer *message, client* clients, int client_amount){
    int sent = 0;
    char final_message[1024+NICK_LEN*2+20], target_client[NICK_LEN];
    generate_private_message(final_message, message->message, message->sender_nickname);
    sscanf(message->message,"%*s %s", target_client);

    if (strcmp(message->sender_nickname, target_client) != 0){
        pthread_mutex_lock(&client_list_mutex);
        for (int i = 0; i < client_amount && !sent; i++) {
            if (clients[i].alive){
                if (strcmp(clients[i].nickname, target_client) == 0){
                    send( *(clients[i].socket), final_message, strlen(final_message)+1, 0);
                    sent++;
                }
            }
        }
        pthread_mutex_unlock(&client_list_mutex);
    }
    send(*(message->sender_socket) ,final_message, strlen(final_message)+1, 0);
}

void remove_client(client* clients, shared_buffer *buffer, int client_amount){
    printf("Sender: se comenzo a remover un cliente\n");
    char *target_client = buffer->sender_nickname_copy;
    pthread_mutex_lock(&client_list_mutex);
    printf("Sender: se tomo el mutex, hay que buscar al cliente\n");
    for (int i = 0; i < client_amount; ++i) {
        if (strcmp(clients[i].nickname, target_client) == 0){
            clients[i].alive = 0;
            printf("Sender:Se removio el cliente %s \n", clients[i].nickname);
        }
    }
    pthread_mutex_unlock(&client_list_mutex);
}

void change_nickname(client* clients, shared_buffer *buffer, int client_amount){
    int *sender_socket = buffer->sender_socket, nickname_available = 1;
    char new_nickname[NICK_LEN], *message = buffer->message;
    sscanf(message,"%*s %s", new_nickname);
    pthread_mutex_lock(&client_list_mutex);
    for (int i = 0; i < client_amount && nickname_available; ++i) {
        if (clients[i].alive){
            if (strcmp(new_nickname, clients[i].nickname) == 0){
                send(*sender_socket, "Nickname en uso, no se efectuo cambio", sizeof("Nickname en uso, no se efectuo cambio"), 0);
                nickname_available = 0;
            }
        }
    }
    pthread_mutex_unlock(&client_list_mutex);
    if(nickname_available){
        strcpy(buffer->sender_nickname, new_nickname);
    }


}

void process_message(sender_data args){
    char* message = args.buffer->message, final_message[1024+NICK_LEN+10];
    if (message[0] == '/'){
        if (strncmp(message, "/msg ", 5) == 0)
            private_message( args.buffer, args.clients, *(args.client_amount));
        else if (strncmp(message, "/exit", 5) == 0)
            remove_client(args.clients, args.buffer, *(args.client_amount));
        else if (strncmp(message, "/nickname ", 10) == 0)
            change_nickname(args.clients, args.buffer, *(args.client_amount));
    }
    else{
        generate_final_message(final_message, message, args.buffer->sender_nickname);
        pthread_mutex_lock(&client_list_mutex);
        printf("Sender: se va a enviar '%s'\n", final_message);
        for (int i = 0; i < *(args.client_amount); i++) {
            if (args.clients[i].alive){
                printf("Sender: se va a enviar '%s' a '%s'\n", final_message, args.clients[i].nickname);
                send(*(args.clients[i].socket), final_message, strlen(final_message)+1, 0);
            }
        }
        pthread_mutex_unlock(&client_list_mutex);
    }
}

void wait_for_message(shared_buffer *buffer){
    while(buffer->message_flag == 0) {
        printf("Sender se duerme\n");
        pthread_cond_wait( &new_message, &shared_buffer_mutex);
        printf("Sender se despierta\n"); 
    }
}
/*Sender*/
void *sender(void *arg){
    sender_data args = *(sender_data*) arg;
    printf("Empezo el sender \n");

    for (;;){
        pthread_mutex_lock(&shared_buffer_mutex);
        printf("Sender toma el mutex\n");
        wait_for_message(args.buffer);
        printf("Sender procesa mensaje\n");
        process_message(args);
        args.buffer->message_flag = 0;
        pthread_mutex_unlock(&shared_buffer_mutex);
    }

}

void prepare_buffer(shared_buffer* sbuffer, char *new_message, char *sender_nickname, int *continue_flag, int* sender_socket){
    strcpy((sbuffer->message), new_message);
    sbuffer->sender_nickname = sender_nickname;
    sbuffer->continue_flag = continue_flag;
    sbuffer->sender_socket = sender_socket;
    strcpy((sbuffer->sender_nickname_copy), sender_nickname);
}

int check_nicknames(char *buf, client* client_list, int client_amount){
    pthread_mutex_lock(&client_list_mutex);
    printf("comparando nicknames\n");
    for (int i = 0; i < client_amount; ++i) {
        if (client_list[i].alive){
            if (strcmp(buf, client_list[i].nickname) == 0){
                pthread_mutex_unlock(&client_list_mutex);
                return 0;
            }
        }
    }
    pthread_mutex_unlock(&client_list_mutex);
    return 1;

}

void store_client(client* client_list, receiver_data* recv_data, int* client_amount){
    int not_stored = 1;
    pthread_mutex_lock(&client_list_mutex);
    for (int i = 0; i < *client_amount && not_stored; i++) {
        if (!(client_list[i].alive)) {
            client_list[i].socket = recv_data->socket;
            client_list[i].alive = 1;
            client_list[i].nickname = recv_data->nickname;
            not_stored = 0;
        }
    }
    if (not_stored) {
        client_list[*client_amount].socket = recv_data->socket;
        client_list[*client_amount].alive = 1;
        client_list[*client_amount].nickname = recv_data->nickname;
        (*client_amount)++;
    }
    pthread_mutex_unlock(&client_list_mutex);
}

/*Receivers*/
void *receiver(void *arg){
    receiver_data args = *(receiver_data*) arg;
    int *socket = args.socket, read_bytes, receive_flag = 1, 
        nickname_not_set = 1;
    char buf[1024], nick_buf[NICK_LEN];

    while(nickname_not_set){
        printf("Chequeando nickname\n");
        send(*socket, "Ingrese su nickname\0", sizeof("Ingrese su nickname\0"), 0);
        printf("Pidio nickname\n");
        read_bytes = recv(*socket, nick_buf, sizeof(nick_buf), 0);
        printf("Leyo nickname %s\n", nick_buf);
        if (read_bytes < 0){
            perror ("receiver_read");
            exit (EXIT_FAILURE);
        }
        if(check_nicknames(nick_buf, args.clients, *(args.client_amount)) == 1) {
            printf("Nickname valido '%s'\n", nick_buf);
            strcpy(args.nickname, nick_buf);
            nickname_not_set = 0;
            if (strcmp(nick_buf, "/exit") == 0)
                receive_flag = 0;
            send(*socket, "Nickname aceptado\0", sizeof("Nickname aceptado\0"), 0);
        }
        else{
            send(*socket, "Nickname en uso\0", sizeof("Nickname en uso\0"), 0);
        }
    }

    if (receive_flag)
        store_client(args.clients, &args, (args.client_amount));

    while (receive_flag) {
        read_bytes = recv(*socket, buf, sizeof(buf), 0);
        printf("Receiver: Leyo mensaje '%s'\n", buf);
        if (read_bytes < 0){
            perror ("receiver_read");
            exit (EXIT_FAILURE);
        }
        else if (read_bytes > 0) {
            buf[read_bytes] = 0;
            pthread_mutex_lock(&shared_buffer_mutex);
            prepare_buffer(args.buffer, buf, args.nickname, &receive_flag, socket);
            args.buffer->message_flag = 1;
            pthread_cond_signal(&new_message);
            pthread_mutex_unlock(&shared_buffer_mutex);
            if (strncmp(buf, "/exit", 5) == 0)
                receive_flag = 0;
        }
    }

    printf("Receiver: Se dejo de escuchar\n");
    close(*socket);
    free(args.socket);
    free((receiver_data*) arg);
    return NULL;
}

void initialize_receiver_data(int* soclient, shared_buffer* buffer, receiver_data** recv_data, 
                                client *client_list, int *client_amount){
    receiver_data* new_receiver;
    
    new_receiver = malloc(sizeof(receiver_data));
    *recv_data = new_receiver;
    
    new_receiver->socket = soclient;

    new_receiver->buffer = buffer;
    new_receiver->clients = client_list;
    new_receiver->client_amount = client_amount;

} 


/* Definimos una pequeña función auxiliar de error */
void error(char *msg){
    exit((perror(msg), 1));
}

int main(int argc, char **argv){
    int sock, *soclient, client_amount;
    struct sockaddr_in servidor, clientedir;
    socklen_t clientelen;
    pthread_t thread;
    pthread_attr_t attr;
    receiver_data *recv_data;

    if (argc <= 1) error("Faltan argumentos");

  /* Creamos el socket */
    if( (sock = socket(AF_INET, SOCK_STREAM, 0)) < 0 )
        error("Socket Init");

  /* Creamos a la dirección del servidor.*/
    servidor.sin_family = AF_INET; /* Internet */
    servidor.sin_addr.s_addr = INADDR_ANY; /**/
    servidor.sin_port = htons(atoi(argv[1]));

    if (bind(sock, (struct sockaddr *) &servidor, sizeof(servidor)))
        error("Error en el bind");

    printf("Binding successful, and listening on %s\n",argv[1]);

    /************************************************************/
    /* Creamos los atributos para los hilos.*/
    pthread_attr_init(&attr);
    /* Hilos que no van a ser *joinables* */
    pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
    /************************************************************/

    /*Inicializar la lista, el sender, buffer y eso*/
    shared_buffer buffer;
    buffer.message_flag = 0;
    client client_list[MAX_CLIENTS*2];
    sender_data sender_args;
    sender_args.buffer = &buffer;
    sender_args.clients = client_list;
    sender_args.client_amount = &client_amount;
    //Lanzar el hilo del sender
    pthread_create(&thread , NULL , sender, (void *) &sender_args);

    if(listen(sock, MAX_CLIENTS) == -1)
        error(" Listen error ");

    for(;;){ /* Comenzamos con el bucle infinito*/
        /* Pedimos memoria para el socket */
        soclient = malloc(sizeof(int));

        /* Now we can accept connections as they come*/
        clientelen = sizeof(clientedir);
        if ((*soclient = accept(sock
                              , (struct sockaddr *) &clientedir
                              , &clientelen)) == -1)
            error("No se puedo aceptar la conexión. ");


        //Inicializamos el receiver
        initialize_receiver_data(soclient, &buffer, &recv_data, client_list, &client_amount);
        
        /* Le enviamos el socket al hijo*/
        pthread_create(&thread , NULL , receiver, (void *) recv_data);
    }

  /* Código muerto */
    close(sock);

    return 0;
}


//TO DO:Revisar regiones criticas respecto a clientes.
//TO DO:Implementar bien lo de arriba (Readers-writers problem)
/*No tuve tiempo de corregir lo mencionado en el TO DO anterior.
El problema de la implementacion actual es que es ineficiente, ya que todos los reader pueden leer al mismo
tiempo, y el unico que necesita exclusividad es el writer. Creo que para el scope del ejercicio la 
implementacion esta es suficiente. No obstante, lo que queria implementar era la solucion descripta
para el Readers-writers problem en el little boof of semaphores. Me parecio relevante mencionarlo */
