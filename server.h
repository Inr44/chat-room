/* RemoteMultiThreadServer.c */
/* Cabeceras de Sockets */
#include <sys/types.h>
#include <sys/socket.h>
/* Cabecera de direcciones por red */
#include <netinet/in.h>
/**********/
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
/**********/
/* Threads! */
#include <pthread.h>

#include "GList.h"

/* Maxima cantidad de cliente que soportará nuestro servidor */
#define MAX_CLIENTS 25
#define NICK_LEN 32

struct _client{
    int* socket;
    int alive;
    char* nickname;
};

typedef struct _client client;

struct _shared_buffer{
    char message[1024];
    char *sender_nickname;
    int message_flag;
    int *continue_flag;
    int *sender_socket;
    char sender_nickname_copy[NICK_LEN];
};

//Se guarda una copia del nickname para usar en la funcion remover.
//Originalmente el sender se encargaba de apagar el loop del receiver cuando recibia el exit, pero creo
//que eso generaba problemas porque el socket se cerraba de un lado (el lado del cliente) mientras el
//receiver seguia escuchando y funcionaba de forma rara. Asi que hago que el receiver interrupa su propia
//ejecucion, mientras el sender termina de arreglar cosas de fondo.

typedef struct _shared_buffer shared_buffer;

struct _receiver_data{
    shared_buffer* buffer;
    int* socket;
    char nickname[NICK_LEN];
    client* clients;
    int* client_amount;
};

typedef struct _receiver_data receiver_data;

struct _sender_data {
    shared_buffer* buffer;
    client* clients;
    int* client_amount;
};

typedef struct _sender_data sender_data;